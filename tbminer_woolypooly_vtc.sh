#!/bin/bash

RESTART="TRUE"
VTC_WALLET="vtc1qm7fpu7z8ukf758ra55pp5lh7xrtqknwkhlgd22"
WORKER_NAME="den"

cleanup ()
{
exit 0
}

trap cleanup SIGINT SIGTERM

while [ 1 ]
do
	./TBMiner --algo verthash --hostname pool.eu.woolypooly.com --port 3102 --wallet $VTC_WALLET --worker-name $WORKER_NAME

    if [ $RESTART == "FALSE" ]
    then
        exit 0
    fi
done
